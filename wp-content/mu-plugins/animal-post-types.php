<?php
// if you want to see full list off edits just google register_post_type shows all parameters you can customise
function animal_post_types(){
    register_post_type('animal', array(
        'public' => true,
        'labels' => array(
            'name' => 'Animals',
            'add_new_item' => 'Add New Animal',
            'edit_item' => 'Edit Animal',
            'all_items' => 'All Animals',
            'singular_name' => 'Animal'
        ),
        'menu_icon' => 'dashicons-pets'
    ));
}

add_action('init', 'animal_post_types')

?>