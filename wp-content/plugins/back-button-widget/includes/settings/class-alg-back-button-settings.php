<?php
/**
 * Back Button Widget - Settings.
 *
 * @version 1.3.0
 * @since   1.3.0
 *
 * @author  Algoritmika Ltd.
 *
 * @see     https://codex.wordpress.org/Creating_Options_Pages
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'Alg_Back_Button_Settings' ) ) :

class Alg_Back_Button_Settings {

	/**
	 * Holds the values to be used in the fields callbacks.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	private $options;

	/**
	 * Constructor.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 *
	 * @todo    [later] add "reset settings" link
	 */
	function __construct() {
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page (under "Settings").
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function add_plugin_page() {
		add_options_page(
			__( 'Back Button Settings', 'back-button-widget' ),
			__( 'Back Button', 'back-button-widget' ),
			'manage_options',
			'alg-back-button-settings',
			array( $this, 'create_admin_page' )
		);
	}

	/**
	 * Options page callback.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function create_admin_page() {
		$this->options = get_option( 'alg_back_button', array() );
		echo '<div class="wrap">' . '<h1>' . __( 'Back Button Settings', 'back-button-widget' ) . '</h1>' . '<form method="post" action="options.php">';
		settings_fields( 'alg_back_button_group' );
		do_settings_sections( 'alg-back-button-settings' );
		submit_button();
		echo '</form>' . '</div>';
	}

	/**
	 * Register and add settings.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function page_init() {
		register_setting(
			'alg_back_button_group',
			'alg_back_button',
			array( $this, 'sanitize' )
		);

		add_settings_section(
			'alg_back_button_section_widget',
			__( 'Widget', 'back-button-widget' ),
			array( $this, 'print_widget_section_info' ),
			'alg-back-button-settings'
		);

		add_settings_section(
			'alg_back_button_section_shortcode',
			__( 'Shortcode', 'back-button-widget' ),
			array( $this, 'print_shortcode_section_info' ),
			'alg-back-button-settings'
		);

		add_settings_section(
			'alg_back_button_section_menu',
			__( 'Menu Options', 'back-button-widget' ),
			array( $this, 'print_menu_section_info' ),
			'alg-back-button-settings'
		);

		add_settings_field(
			'menu_enabled',
			__( 'Enable section', 'back-button-widget' ),
			array( $this, 'menu_enabled_callback' ),
			'alg-back-button-settings',
			'alg_back_button_section_menu'
		);

		add_settings_field(
			'menu_theme_locations',
			__( 'Menu location(s)', 'back-button-widget' ),
			array( $this, 'menu_theme_locations_callback' ),
			'alg-back-button-settings',
			'alg_back_button_section_menu'
		);

		add_settings_field(
			'menu_ids',
			__( 'Menu(s)', 'back-button-widget' ),
			array( $this, 'menu_ids_callback' ),
			'alg-back-button-settings',
			'alg_back_button_section_menu'
		);

		add_settings_field(
			'menu_button',
			__( 'Button', 'back-button-widget' ),
			array( $this, 'menu_button_callback' ),
			'alg-back-button-settings',
			'alg_back_button_section_menu'
		);

		add_settings_field(
			'menu_item_template',
			__( 'Item template', 'back-button-widget' ),
			array( $this, 'menu_item_template_callback' ),
			'alg-back-button-settings',
			'alg_back_button_section_menu'
		);

		add_settings_field(
			'menu_replace_url_enabled',
			__( 'Replace URL', 'back-button-widget' ),
			array( $this, 'menu_replace_url_enabled_callback' ),
			'alg-back-button-settings',
			'alg_back_button_section_menu'
		);
	}

	/**
	 * Sanitize each setting field as needed.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 *
	 * @param   array $input Contains all settings fields as array keys
	 */
	function sanitize( $input ) {
		$new_input = array();
		if ( isset( $input['menu_enabled'] ) ) {
			$new_input['menu_enabled'] = sanitize_text_field( $input['menu_enabled'] );
		}
		if ( isset( $input['menu_theme_locations'] ) ) {
			$new_input['menu_theme_locations'] = array_map( 'sanitize_text_field', $input['menu_theme_locations'] );
		}
		if ( isset( $input['menu_ids'] ) ) {
			$new_input['menu_ids'] = array_map( 'sanitize_text_field', $input['menu_ids'] );
		}
		if ( isset( $input['menu_button'] ) ) {
			$new_input['menu_button'] = wp_kses_post( trim( wp_unslash( $input['menu_button'] ) ) );
		}
		if ( isset( $input['menu_item_template'] ) ) {
			$new_input['menu_item_template'] = wp_kses_post( trim( wp_unslash( $input['menu_item_template'] ) ) );
		}
		if ( isset( $input['menu_replace_url_enabled'] ) ) {
			$new_input['menu_replace_url_enabled'] = sanitize_text_field( $input['menu_replace_url_enabled'] );
		}
		return $new_input;
	}

	/**
	 * Print the Section text.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function print_widget_section_info() {
		echo '<p>' .
			sprintf( __( 'The plugin creates "%s" widget in %s. You can add and configure it there.', 'back-button-widget' ),
				__( 'Back Button', 'back-button-widget' ),
				'<a target="_blank" href="' . admin_url( 'widgets.php' ) . '">' . __( 'Appearance > Widgets', 'back-button-widget' ) . '</a>' ) .
		'</p>';
	}

	/**
	 * Print the Section text.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 *
	 * @todo    [maybe] mention `alg_back_button()` function as well?
	 */
	function print_shortcode_section_info() {
		echo '<p>' .
			sprintf( __( 'You can also add the button anywhere on your site with %s shortcode, e.g.: %s', 'back-button-widget' ),
				'<code>[alg_back_button]</code>',
				'<pre style="color:#444444;background-color:#e0e0e0;padding:10px;">[alg_back_button label="Go back"]</pre>' ) .
		'</p>';
	}

	/**
	 * Print the Section text.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function print_menu_section_info() {
		echo '<p>' .
			__( 'Here you can add the back button to your menu(s).', 'back-button-widget' ) .
		'</p>';
	}

	/**
	 * Get the settings option array and print one of its values.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function menu_enabled_callback() {
		$saved_value = ( isset( $this->options['menu_enabled'] ) ? esc_attr( $this->options['menu_enabled'] ) : 'no' );
		echo '<select id="menu_enabled" name="alg_back_button[menu_enabled]"' . apply_filters( 'alg_back_button_widget_settings', ' disabled' ) . '>' .
				'<option value="no"'  . selected( $saved_value, 'no',  false ) . '>' . __( 'No',  'back-button-widget' ) . '</option>' .
				'<option value="yes"' . selected( $saved_value, 'yes', false ) . '>' . __( 'Yes', 'back-button-widget' ) . '</option>' .
			'</select>' .
			apply_filters( 'alg_back_button_widget_settings', '<p style="background-color:white;padding:10px;">' .
				'You will need <a target="_blank" href="https://wpfactory.com/item/back-button-widget-wordpress-plugin/">Back Button Widget Pro</a> plugin to enable this section.' .
			'</p>' );
	}

	/**
	 * Get the settings option array and print one of its values.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 *
	 * @todo    [next] select2
	 * @todo    [next] check if `! empty( $select_options )`?
	 */
	function menu_theme_locations_callback() {
		$saved_values   = ( isset( $this->options['menu_theme_locations'] ) ? array_map( 'esc_attr', $this->options['menu_theme_locations'] ) : array() );
		$select_options = '';
		foreach ( get_registered_nav_menus() as $key => $value ) {
			$select_options .= '<option value="' . $key . '"' . selected( in_array( $key, $saved_values ), true, false ) . '>' . $value . '</option>';
		}
		echo '<select class="widefat" multiple id="menu_theme_locations" name="alg_back_button[menu_theme_locations][]">' . $select_options . '</select>';
		echo '<p>' .
			sprintf( __( 'Alternatively you can use the "%s" option.', 'back-button-widget' ), __( 'Menu(s)', 'back-button-widget' ) ) . '<br>' .
			sprintf( __( 'You can select multiple menus by holding %s key.', 'back-button-widget' ), '<code>' . __( 'Ctrl', 'back-button-widget' ) . '</code>' ) . '<br>' .
			__( 'Button will be added as the last item in selected menu location(s).', 'back-button-widget' ) .
		'</p>';
	}

	/**
	 * Get the settings option array and print one of its values.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 *
	 * @todo    [next] select2
	 * @todo    [next] check if `! empty( $select_options )`?
	 */
	function menu_ids_callback() {
		$saved_values   = ( isset( $this->options['menu_ids'] ) ? array_map( 'esc_attr', $this->options['menu_ids'] ) : array() );
		$select_options = '';
		foreach ( wp_get_nav_menus() as $term ) {
			$select_options .= '<option value="' . $term->term_id . '"' . selected( in_array( $term->term_id, $saved_values ), true, false ) . '>' . $term->name . '</option>';
		}
		echo '<select class="widefat" multiple id="menu_ids" name="alg_back_button[menu_ids][]">' . $select_options . '</select>';
		echo '<p>' .
			sprintf( __( 'Alternatively you can use the "%s" option.', 'back-button-widget' ), __( 'Menu location(s)', 'back-button-widget' ) ) . '<br>' .
			sprintf( __( 'You can select multiple menus by holding %s key.', 'back-button-widget' ), '<code>' . __( 'Ctrl', 'back-button-widget' ) . '</code>' ) . '<br>' .
			__( 'Button will be added as the last item in selected menu(s).', 'back-button-widget' ) .
		'</p>';
	}

	/**
	 * Get the settings option array and print one of its values.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function menu_button_callback() {
		$value = ( isset( $this->options['menu_button'] ) ? esc_attr( $this->options['menu_button'] ) : '' );
		echo '<input class="widefat" type="text" id="menu_button" name="alg_back_button[menu_button]" value="' . $value . '" />' .
			'<p>' . sprintf( __( 'If empty, then the default value will be used: %s.', 'back-button-widget' ),
				'<code>' . esc_html( '[alg_back_button label="Back" type="simple"]' ) . '</code>' ) . '</p>';
	}

	/**
	 * Get the settings option array and print one of its values.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 */
	function menu_item_template_callback() {
		$value = ( isset( $this->options['menu_item_template'] ) ? esc_attr( $this->options['menu_item_template'] ) : '' );
		echo '<input class="widefat" type="text" id="menu_item_template" name="alg_back_button[menu_item_template]" value="' . $value . '" />' .
			'<p>' . sprintf( __( 'If empty, then the default value will be used: %s.', 'back-button-widget' ),
				'<code>' . esc_html( '<li>%button%<li>' ) . '</code>' ) . '</p>';
	}

	/**
	 * Get the settings option array and print one of its values.
	 *
	 * @version 1.3.0
	 * @since   1.3.0
	 *
	 * @todo    [maybe] better desc?
	 */
	function menu_replace_url_enabled_callback() {
		$saved_value = ( isset( $this->options['menu_replace_url_enabled'] ) ? esc_attr( $this->options['menu_replace_url_enabled'] ) : 'no' );
		echo '<select id="menu_replace_url_enabled" name="alg_back_button[menu_replace_url_enabled]">' .
				'<option value="no"'  . selected( $saved_value, 'no',  false ) . '>' . __( 'No',  'back-button-widget' ) . '</option>' .
				'<option value="yes"' . selected( $saved_value, 'yes', false ) . '>' . __( 'Yes', 'back-button-widget' ) . '</option>' .
			'</select>' .
			'<p>' . sprintf( __( 'This is an alternative method for adding the back button: add a "Custom Link" to your menu(s) (in %s), and include %s text in the "URL".', 'back-button-widget' ),
				'<a target="_blank" href="' . admin_url( 'nav-menus.php' ) . '">' . __( 'Appearance > Menus', 'back-button-widget' ) . '</a>',
				'<code>' . esc_html( '[ALG_BACK_BUTTON]' ) . '</code>' ) . '</p>';
	}

}

endif;

return new Alg_Back_Button_Settings();
